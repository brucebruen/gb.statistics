# gb.statistics
A proposed Gambas 3 component that provides some basic statistics.

The **Stats** module provides a set of basic statistics methods for analyzing one or two sets of data.

All the methods are public so that further, more sophisticated statistical treatments can be done at the client level. For example, the sum of the squares of the deviations from the mean :
    `SumSqrDev= Σ[Xj - μ]`
is used commonly in many statistics formulae. Thus it is public, even if not apparently useful.↓

The following functions are provided:

**AdjR2**   Returns the "adjusted R-squared" value for the data sets.

**CoVar**	Returns the co-variance of the two data arrays

**Correl**	Returns the correlation coefficient for the two data sets,
If the data sets are not of equal sizes then an error is raised.

**Dev**	Returns the deviation of a sample from the mean (X - μ̅̅)

**Histo**	Returns a histogram of the data array. The return value consists of an array of two arrays, the first being the set of "bins" that were used to count the values and the second being the "tallies of those values. The method takes and optional second parameter that :determines whether the returned data is sorted by the "bin" order or not.
_Note_, the usefulness of this method is dependant on whether you have a data set with reasonably discrete values. In other words, just because the data parameter is a Float[] don't expect magic to happen. The data values are treated as discrete. Bins are not ranges like in some spreadsheets.

**HistoS**	

**LinReg**	Returns the linear regression co-efficients (m,b,r2) for the two data sets as an array of floats.

**Max**	Returns the maximum value in the data array

**Mean**	Returns the mean value (μ) of the values in the data array.
If the array is empty the function returns 0. 9Takes one data array.)

**Median**	Returns the median value of the data set.

**Min**	Returns the minimum value in the data array

**Mode**	Returns an array of the modal values of the data set.

**Range**	Returns the absolute range of the data array

**Rank**	

**RankArray**	

**Samples**	Returns the number of items in the data array. (included for convenience)

**Skew**	Returns the skewness of the data set

**StdDev**	Returns the (population) standard deviance of the data array

**Sum**	Returns the sum of the data array.

**SumProd**	Returns the product sum of the two data sets. SumProd() = Σi=0->n(X Y)

**SumSqr**	Returns the sum of squares of the data array. If the array is empty then 0 is returned.

**SumSqrDev**	(dev) Returns the sum of squares of deviations from the mean of the data array values by a different mechanism to avoid the possibility of an irrational mean. For example Sum(data)=17 and Samples(data)=7 then the mean is 17/7 which is an irrational number. It's probably only and issue with extreme data sets By that I mean the data set is too small or the data set values are too extreme.

**Var**	Returns the (population) variance of the data array

**WAvg**	Returns the weighted average value of the data array. The weights are applied by default to the array members with lower indices.

**WAvgR**	Returns the weighted average value of the data array. The weights are applied by default to the array members with higher indices.
